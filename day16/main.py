import binascii

data_hex = "38006F45291200"
data = bin(int(data_hex, 16))[2:].zfill(8)


i = 0
while i < len(data):
    # waiting for a new package
    packetVersion = data[i] + data[i+1] + data[i+2]

    if packetVersion == "110":
        print("new package version " + str(int(packetVersion, 2)) + " found")
        typeID = data[i+3] + data[i+4] + data[i+5]

        if typeID == "100":
            print("new id " + + str(int(typeID, 2)) + + " found")
            i = i+6
            while True:
                print("found:", str(data[i+1]+data[i+2]+data[i+3]+data[i+4]))
                if data[i] == "0":
                    break
                i += 5
            print("end of package version 6")
            i += 5
        else:
            print("new id " + + str(int(typeID, 2)) + + " found")
            length_type_id = data[i+6]
            if length_type_id == "0":
