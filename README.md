# Advent Of Code 2021
Advent Of Code is a daily challenge for programmers to solve programming problems. The difficulty can range from easy to very complex.
For more information, visit https://adventofcode.com/

## My participation
This is the first time I heard about such an event, which is known by many developers internationally. When I had the time, I tried to solve some of the puzzles, which led to good success. 

## Techstack
Depending on the problem, I used C++ or Python for the projects. 
