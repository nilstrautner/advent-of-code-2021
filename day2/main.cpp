#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <queue>

using namespace std;

int pos = 0;
int depth = 0;
int aim = 0;

void readData() {
    //Read file
    string line;
    ifstream myfile ("../day2/data.txt");
    if (myfile.is_open()) {
        while (getline (myfile,line)) {
            if (line.at(0) == 'f') {
                pos += line.at(8) - '0';
                depth += aim * (line.at(8) - '0');
            }
            if (line.at(0) == 'd') aim += line.at(5) - '0';
            if (line.at(0) == 'u') aim -= line.at(3) - '0';
            cout << line << ":\t" << aim << " " << pos << " " << depth << endl;
        }
        myfile.close();
    } else cout << "Unable to open file";
}

int main() {
    readData();
    cout << "Position: " << pos << endl;
    cout << "Depth: " << depth << endl;
    cout << "Ergebnis: " << pos*depth << endl;
}
