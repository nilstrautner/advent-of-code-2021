#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <queue>

using namespace std;


struct Vertex {
    int wert;
    int dist = 99;
    Vertex* prev = nullptr;
};

int w = 10, h = 10;
vector<vector<Vertex*>> graph;


void readData() {
    //Init Array
    for (int y = 0; y < h; y++) {
        vector<Vertex*> temp;
        for (int x = 0; x < w; x++) {
            Vertex* v;
            v->wert = -1;
            temp.push_back(v);
        }
        graph.push_back(temp);
        temp.clear();
    }

    //Read file
    string line;
    ifstream myfile ("../day15v2/data.txt");
    if (myfile.is_open()) {
        int y = 0;
        while (getline (myfile,line)) {
            std::stringstream ss(line);
            for (int i = 0; i < line.length(); i++) {
                Vertex* v;
                v.wert = line.at(i) - '0';
                if (line.at(i) != '\n') graph[i][y] = v;
            }
            y++;
        }
        myfile.close();
    } else cout << "Unable to open file";
}

void print() {
    for (int y = 0; y < graph.size(); y++) {
        for (int x = 0; x < graph[y].size(); x++) {
            cout << graph[x][y]->wert << " ";
        }
        cout << endl;
    }
}

void printDist() {
    cout << "Dist von 0, 0 zu jedem Punkt:" << endl;
    for (int y = 0; y < graph.size(); y++) {
        for (int x = 0; x < graph[y].size(); x++) {
            cout << graph[x][y]->dist << " ";
        }
        cout << endl;
    }
}

void printWay(int endX, int endY) {
    int prevX = 0, prevY = 0;
    cout << "\nWeg von hinten nach vorne: ";
    while (prevX != -1 && prevY != -1) {
        cout << graph[endX][endY]->wert << " ";
        prevX = graph[endX][endY]->prevX;
        prevY = graph[endX][endY]->prevY;
    }
}

Vertex* getSmallest(vector<Vertex*> Q) {
    Vertex* smallest = Q.at(0);
    int index = 0;
    for (int i = 0; i < Q.size(); i++) {
        if (Q.at(i)->dist < smallest->dist) {
            smallest = Q.at(i);
            index = i;
        }
    }
    Q.erase(Q.begin() + index);
    return smallest;
}

void visit(vector<int> u, int vx, int vy) {
    if (graph[vx][vy].visited) return;
    int alt = graph[u[0]][u[1]].dist + (graph[vx][vy].wert);
    if (alt < graph[vx][vy].dist) {
        graph[vx][vy].dist = alt;
        graph[vx][vy].prevX = u[0];
        graph[vx][vy].prevY = u[1];
    }
}

void dijkstra(Vertex* start) {
    vector<Vertex*> Q;
    for (int y = 0; y < graph.size(); y++) {
        for (int x = 0; x < graph[y].size(); x++) {
            Vertex* v = graph[x][y];
            v->dist = 100000;
            v->prev = nullptr;
            if (v != start) Q.push_back(v);
        }
    }
    start->dist = 0;

    while (Q.size() > 0) {
        Vertex* u = getSmallest(Q);

    }
}

int main()
{
    readData();

    print();
    dijkstra(graph[0][0]);
    printDist();
    //cout << "Ergebnis: " << graph[w-1][h-1].dist << endl;

    //printWay(9,9);
    return 0;
}
