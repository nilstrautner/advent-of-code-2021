#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;



vector<int> readData() {
    vector<int> data;
    //Read file
    string line;
    ifstream myfile ("../day1/data.txt");
    if (myfile.is_open()) {
        while (getline (myfile,line)) {
            data.push_back(stoi(line));
        }
        myfile.close();
    } else cout << "Unable to open file";
    return data;
}

int main()
{
    vector<int> data = readData();
    vector<int> threeSumData;

    for (int i = 0; i+2 < data.size(); i++) {
         int sum = data[i] + data[i+1] + data[i+2];
         threeSumData.push_back(sum);
    }

    int incr = 0;
    for (int i = 0; i < threeSumData.size(); i++) {
        cout << threeSumData[i];
         if (i > 0 && threeSumData[i] > threeSumData[i-1]) {
             incr++;
             cout << " (inscreased)";
         }
         cout << endl;
    }
    cout << "Anzahl Erh�hungen: " << incr << endl;

    return 0;
}
