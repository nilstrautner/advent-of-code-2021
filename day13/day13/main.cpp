#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

struct Tuple {
    int x, y;
};

int w = 0, h = 0;
bool map[5000][5000];
vector<Tuple> initPoints;

void readData() {
    //Read file
    string line;
    ifstream myfile ("../day13/data.txt");
    if (myfile.is_open()) {
        while (getline (myfile,line)) {
            std::stringstream ss(line);
            std::vector<int> vect;
            for (int i; ss >> i;) {
                vect.push_back(i);
                if (ss.peek() == ',')
                ss.ignore();
            }
            Tuple t;
            t.x = vect[0];
            t.y = vect[1];
            initPoints.push_back(t);
        }
        myfile.close();
    } else cout << "Unable to open file";
}

void foldY(int i) {
    for (int y = i+1; y < h; y++) {
        for (int x = 0; x < w; x++) {
           if (map[x][y]) map[x][i-(y-i)] = true;
           //if (map[x][y]) cout << x << "/" << y << " auf " << x << "/" << (i-(y-i)) << endl;
        }
    }

    h = h / 2;
}

void foldX(int i) {
    for (int y = 0; y < h; y++) {
        for (int x = i+1; x < w; x++) {
           if (map[x][y]) map[i-(x-i)][y] = true;
           //if (map[x][y]) cout << x << "/" << y << " auf " << (i-(x-i)) << "/" << y << endl;
        }
    }

    w = w / 2;
}

void print() {
    //Print Map
    cout << "Size: w=" << w << " h=" << h << endl;
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            if (map[x][y]) cout << "#"; else cout << ".";
        }
        cout << endl;
    }
}

void initMap() {
    //Map
    for (Tuple t : initPoints) {
        if (t.x > w) w = t.x;
        if (t.y > h) h = t.y;
    }
    w++;
    h++;

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            map[x][y] = false;
        }
    }
    for (Tuple t : initPoints) {
        map[t.x][t.y] = true;
    }
}

int main()
{
    readData();
    initMap();

    //Folds
    foldX(655);
    foldY(447);
    foldX(327);
    foldY(223);
    foldX(163);
    foldY(111);
    foldX(81);
    foldY(55);
    foldX(40);
    foldY(27);
    foldY(13);
    foldY(6);


    //Prints
    print();

    //Count
    int c = 0;
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            if (map[x][y]) c++;
        }
    }
    cout << "Anzahl: " << c << endl;

    return 0;
}
