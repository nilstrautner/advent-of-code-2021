import string

def get_data():
    inp = "BCHCKFFHSKPBSNVVKVSK"
    file = open("rules.txt", "r")
    r = file.read()
    rules = []

    while len(r) > 0:
        part1 = r[0] + r[1]
        part2 = r[6]
        r = r[8:]
        rules.append((part1, part2))
    
    return inp, rules

def get_char(s1, s2):
    s = s1 + s2
    for x in rules:
        if x[0] == s:
            return x[1]
    return "-"

def char_position(letter):
    return ord(letter) - 97

def get_element_count():
    counted = []
    for i in range(27):
        counted.append(0)
    for x in text:
        counted[char_position(x.lower())] += 1
    return counted

text, rules = get_data()
newText = ""

for step in range(10):
    newText = ""
    for i in range(len(text)-1):
        newText += text[i] + get_char(text[i], text[i+1])
        #print("Added " + get_char(text[i], text[i + 1]))
    newText += text[len(text)-1]
    text = newText
    print("After Step", (step+1), len(text))

counted = get_element_count()
min = 100000
for i in counted:
    if i > 0 and i < min:
        min = i

print("Ergebnis: ", max(counted), "-", min, "=", (max(counted)-min))
