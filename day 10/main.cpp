#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>    // std::sort

using namespace std;
using std::ifstream;

char getOpposite(char c) {
    if (c == '(') return ')';
    if (c == '{') return '}';
    if (c == '[') return ']';
    if (c == '<') return '>';
    if (c == ')') return '(';
    if (c == '}') return '{';
    if (c == ']') return '[';
    if (c == '>') return '<';
    return 'X';
}

bool isCorrupted(string line) {
    bool checked[line.length()];
    for (size_t i = 0; i < line.length(); i++) checked[i] = false;

    for (size_t i = 0; i < line.length(); i++) {
        char z = line.at(i);
        if (z == ')' || z == ']' || z == '}' || z == '>') {
            for (size_t ii = i-1; ii >= 0; ii--) {
                if (checked[ii]) continue;
                if (line.at(ii) == getOpposite(z)) {
                    checked[ii] = true;
                    checked[i] = true;
                    break;
                } else {
                    cout << i << " " << ii << " Error found: Expected " << getOpposite(line.at(ii)) << " but found " << z << " instead." << endl;
                    return true;
                }
            }
        }
    }

    return false;
}

string getEnding(string line) {
    bool checked[line.length()];
    for (size_t i = 0; i < line.length(); i++) checked[i] = false;

    for (size_t i = 0; i < line.length(); i++) {
        char z = line.at(i);
        if (z == ')' || z == ']' || z == '}' || z == '>') {
            for (size_t ii = i-1; ii >= 0; ii--) {
                if (checked[ii]) continue;
                if (line.at(ii) == getOpposite(z)) {
                    checked[ii] = true;
                    checked[i] = true;
                    break;
                }
            }
        }
    }

    string result = "";
    for (int i = line.length()-1; i >= 0; i--) {
        if (!checked[i]) {
            result += getOpposite(line.at(i));
        }
    }
    return result;
}

size_t getPoints(string s) {
    size_t points = 0;
    for (char c : s) {
        points *= 5;
        if (c == ')') points += 1;
        if (c == ']') points += 2;
        if (c == '}') points += 3;
        if (c == '>') points += 4;
    }
    return points;
}



int main()
{
    //Testing
    //cout << getEnding("[({(<(())[]>[[{[]{<()<>>") << endl;
    //cout << getPoints("]]}}]}]}>") << endl;

    vector<size_t> results;

    string line;
    ifstream myfile ("../day10/data.txt");
    if (myfile.is_open()) {
        while (getline (myfile,line)) {
          if (isCorrupted(line)) continue;
          //cout << getEnding(line) << endl;
          string s = getEnding(line);
          size_t p = getPoints(s);
          cout << "Autocompletion: " << s << " Points: " << p << endl;
          results.push_back(p);
        }
        myfile.close();
    } else cout << "Unable to open file";

     std::sort(results.begin(), results.end());
     for (size_t i : results) cout << i << endl;
    cout << "Mitte: " << results[results.size() / 2] << endl;

    return 0;
}
