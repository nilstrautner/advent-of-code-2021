#include <iostream>
#include <string>
#include <fstream>

using namespace std;
using std::ifstream;

char getOpposite(char c) {
    if (c == '(') return ')';
    if (c == '{') return '}';
    if (c == '[') return ']';
    if (c == '<') return '>';
    if (c == ')') return '(';
    if (c == '}') return '{';
    if (c == ']') return '[';
    if (c == '>') return '<';
    return 'X';
}

char getFirstIllegalChar(string line) {
    bool checked[line.length()];
    for (size_t i = 0; i < line.length(); i++) checked[i] = false;

    for (size_t i = 0; i < line.length(); i++) {
        char z = line.at(i);
        if (z == ')' || z == ']' || z == '}' || z == '>') {
            for (size_t ii = i-1; ii >= 0; ii--) {
                if (checked[ii]) continue;
                if (line.at(ii) == getOpposite(z)) {
                    checked[ii] = true;
                    checked[i] = true;
                    break;
                } else {
                    cout << i << " " << ii << " Error found: Expected " << getOpposite(line.at(ii)) << " but found " << z << " instead." << endl;
                    return z;
                }
            }
        }
    }

    return 'X';
}

int getPoints(char c) {
    if (c == ')') return 3;
    if (c == '}') return 1197;
    if (c == ']') return 57;
    if (c == '>') return 25137;
    return 0;
}

int main()
{
    int points = 0;
    //points += getPoints(getFirstIllegalChar("<{([{{}}[<[[[<>{}]]]>[]]"));

    string line;
    ifstream myfile ("../day10/data.txt");
    if (myfile.is_open()) {
        while (getline (myfile,line)) {
          points += getPoints(getFirstIllegalChar(line));
        }
        myfile.close();
    } else cout << "Unable to open file";

      cout << "Ergebnis: "  << points << endl;

    return 0;
}
